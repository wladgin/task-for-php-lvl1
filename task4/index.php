<?php
/*На странице есть несколько картинок (от 3-х штук) подсчитывать для каждой картинки количество просмотров картинки
(просмотром считается переход на страницу где отображается одна выбранная картинка). Статистику по просмотрам выводить
на отдельной странице.*/


$directory = "./img";    // Папка с изображениями
$allowed_types = ["jpeg", "jpg", "png"];  //разрешеные типы изображений
$file_parts = [];
$ext = "";
$title = "";

//пробуем открыть папку
$dir_handle = @opendir($directory) or die("Ошибка при открытии папки!");
while (false !== ($file = readdir($dir_handle))) {    //поиск по файлам

    if ($file == "." || $file == "..") continue;  //пропустить ссылки на другие папки
    $file_parts = explode(".", $file);          //разделить имя файла и поместить его в массив
    $ext = strtolower(array_pop($file_parts));   //последний элеменет - это расширение


    if (in_array($ext, $allowed_types)) {
        echo '<div>
                <a href="' . 'picture.php' . '?image=' . '/img' . "/" . $file . '&'. 'name='.$file.'"> ' . $file . '</a>      
            </div>';
    }


}
echo '<a href="store.php">Статистика</a>';
closedir($dir_handle);  //закрыть папку

?>
